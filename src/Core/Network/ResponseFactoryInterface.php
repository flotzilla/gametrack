<?php

declare(strict_types=1);

namespace Core\Network;


interface ResponseFactoryInterface
{
    public function createResponse(int $code = 200): ResponseInterface;
}

<?php

declare(strict_types=1);


namespace Core\Template;


use InvalidArgumentException;

class Template
{
    public const DI = 'Template';

    protected $rootDir = '';

    /**
     * @var string
     */
    protected $openDelimiter = '{{';

    /**
     * @var string
     */
    protected $closeDelimiter = '}}';

    /**
     * Constructor.
     *
     * @param string $templateDir
     * @param string $openDelimiter
     * @param string $closeDelimiter
     */
    public function __construct(string $templateDir, string $openDelimiter = '{{', string $closeDelimiter = '}}')
    {
        $this->rootDir = $templateDir;
        $this->openDelimiter = $openDelimiter;
        $this->closeDelimiter = $closeDelimiter;
    }

    /**
     * Renders the template and returns the result.
     *
     * @param string $fileName
     * @param array $values
     * @return string
     */
    public function render(string $fileName, array $values = [])
    {
        $template = $this->loadFile($fileName);

        $keys = array();

        foreach ($values as $key => $value) {
            $keys[] = $this->openDelimiter . $key . $this->closeDelimiter;
        }

        $template = str_replace($keys, $values, $template);

        // clean up template {{}} directives if values was not set
        return preg_replace('/{{[a-z-A-z0-9]*}}/m', "", $template);
    }

    /**
     * @param string $fileName
     * @return bool
     */
    public function isTemplateExists(string $fileName): bool
    {
        $file = $this->rootDir . $fileName;
        return file_exists($file) && is_readable($file);
    }

    /**
     * Sets the template file.
     *
     * @param  string $file
     * @return string
     * @throws InvalidArgumentException
     */
    public function loadFile(string $file): string
    {
        $template = '';

        $file = $this->rootDir . $file;

        if (file_exists($file) && is_readable($file)) {
            $template = file_get_contents($file);
        } else {
            throw new InvalidArgumentException(
                'Template file could not be loaded.'
            );
        }

        return $template;
    }
}

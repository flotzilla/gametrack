<?php

declare(strict_types=1);

namespace Core\Validator;


/**
 * Interface Validator
 * @package Core\Validator
 */
interface Validator
{
    /**
     * @param array $o
     * @return bool
     */
    public function validate(array $o): bool ;

    /**
     * @return bool
     */
    public function hasErrors(): bool;

    /**
     * @return array
     */
    public function getErrors(): array;

    /**
     * @param string $field
     * @param string $message
     */
    public function addError(string $field, string $message): void;
}

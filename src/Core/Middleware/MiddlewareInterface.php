<?php

declare(strict_types=1);

namespace Core\Middleware;


use Core\Network\RequestInterface;
use Core\Network\ResponseInterface;

interface MiddlewareInterface
{
    public function process(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface;
}

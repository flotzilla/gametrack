<?php

declare(strict_types=1);

namespace Core\Middleware;


use Core\Network\RequestInterface;
use Core\Network\ResponseInterface;

interface DelegateInterface
{
    public function handle(RequestInterface $request): ResponseInterface;
}

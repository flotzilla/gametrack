<?php

declare(strict_types=1);

namespace Core\DataSource;

use MongoDB\Driver\Exception\InvalidArgumentException as DriverInvalidArgumentException;
use MongoDB\Driver\Exception\RuntimeException as DriverRuntimeException;
use MongoDB\Exception\InvalidArgumentException;
use Core\Config\Config;
use Core\DataSource\Exception\WrongConfigException;

interface DataSource
{
    /**
     * @throws InvalidArgumentException for parameter/option parsing errors
     * @throws DriverInvalidArgumentException for parameter/option parsing errors in the driver
     * @throws DriverRuntimeException for other driver errors (e.g. connection errors)
     * @return mixed
     */
    public function getConnection();

    /**
     * @param Config $config
     * @throws WrongConfigException
     * @return mixed
     */
    public function loadConfig(Config $config) ;
}

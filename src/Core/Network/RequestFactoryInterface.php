<?php

declare(strict_types=1);

namespace Core\Network;


interface RequestFactoryInterface
{
    public function createRequest(string $method, string $uri, string $requestParams): RequestInterface;
}

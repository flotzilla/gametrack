<?php

use App\Provider\AppProvider;
use Core\Config\Config;
use Core\Container\Container;
use Core\Service\ServiceProviderInterface;

require_once __DIR__ . '/vendor/autoload.php';

const ROOT_CONFIG = __DIR__ . DIRECTORY_SEPARATOR ;

$env = getenv('APP_ENV') ?: 'dev';
$providers = [
    AppProvider::class,
];
$configPath =  ROOT_CONFIG. 'config' . DIRECTORY_SEPARATOR . 'config.' . $env .'.php';
$config = new Config($configPath, $env);

$container = new Container(
    [
        'Config' => function () use ($config) {return $config;}
    ]
);

foreach ($providers as $providerClassName) {
    /** @var  ServiceProviderInterface $provider */
    $provider = new $providerClassName;
    $provider->register($container);
}

return $container;
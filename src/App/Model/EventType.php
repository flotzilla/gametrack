<?php

declare(strict_types=1);

namespace App\Model;


use MongoDB\BSON\ObjectId;
use MongoDB\BSON\Serializable;
use MongoDB\BSON\Unserializable;

/**
 * Class EventType
 * @package App\Model
 */
class EventType implements Serializable, Unserializable
{
    /** @var ObjectId $id */
    private $id;

    /** @var string $name */
    private $name = '';

    /** @var string $description */
    private $description = '';

    /**
     * EventType constructor.
     * @param string $name
     * @param string $description
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     */
    public function __construct(string $name, string $description)
    {
        $this->id = new ObjectId;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return ObjectId
     */
    public function getId(): ObjectId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return EventType
     */
    public function setName(string $name): EventType
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return EventType
     */
    public function setDescription(string $description): EventType
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Provides an array or document to serialize as BSON
     * Called during serialization of the object to BSON. The method must return an array or stdClass.
     * Root documents (e.g. a MongoDB\BSON\Serializable passed to MongoDB\BSON\fromPHP()) will always be serialized as a BSON document.
     * For field values, associative arrays and stdClass instances will be serialized as a BSON document and sequential arrays (i.e. sequential, numeric indexes starting at 0) will be serialized as a BSON array.
     * @link https://php.net/manual/en/mongodb-bson-serializable.bsonserialize.php
     * @return array|object An array or stdClass to be serialized as a BSON array or document.
     */
    public function bsonSerialize()
    {
        return [
            '_id' => $this->id,
            'name' => $this->name,
            'description' => $this->description
        ];
    }

    /**
     * Constructs the object from a BSON array or document
     * Called during unserialization of the object from BSON.
     * The properties of the BSON array or document will be passed to the method as an array.
     * @link https://php.net/manual/en/mongodb-bson-unserializable.bsonunserialize.php
     * @param array $data Properties within the BSON array or document.
     */
    public function bsonUnserialize(array $data)
    {
        $this->id = $data['_id'];
        $this->name = $data['name'] ?: '';
        $this->description = $data['description'] ?: '';
    }
}

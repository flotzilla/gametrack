<?php
declare(strict_types=1);


namespace App\Validator;


use Core\Validator\Validator;

/**
 * Class EventValidator
 * @package App\Validator
 */
class EventValidator implements Validator
{

    protected $errors = [];

    /**
     * @param array $a
     * @return bool
     */
    public function validate(array $a): bool
    {
        if (!array_key_exists('eventType', $a) || strlen($a['eventType']) != 24){
            $this->addError('eventType', 'is not a valid Id');
        }

        if (!array_key_exists('device', $a) || empty($a['device'])){
            $this->addError('device', 'field required');
        }

        if (!array_key_exists('acquisitionSource', $a) || empty($a['acquisitionSource'])){
            $this->addError('acquisitionSource', 'field required');
        }

        if (!is_array($a['data'])){
            $this->addError('data', 'wrong format');
        }

        return !$this->hasErrors();
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return count($this->errors) > 0;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param string $field
     * @param string $message
     * @return mixed
     */
    public function addError(string $field, string $message): void
    {
        $this->errors[] = [
            'field' => $field,
            'message' => $message
        ];
    }
}

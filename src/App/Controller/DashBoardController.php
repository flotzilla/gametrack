<?php

declare(strict_types=1);

namespace App\Controller;


use Core\Controller\Http\Controller;

class DashBoardController extends Controller
{
    public function indexAction()
    {
        return $this->template
            ->render('base.php', [
                    'title' => 'Dashboard',
                    'content' => $this->template->render('dashboard.php', [
                        'x' => 3221
                    ])
                ]
            );
    }

    public function viewAction()
    {
        return $this->template->render('base.php', [
            'content' => 'View action'
        ]);
    }
}

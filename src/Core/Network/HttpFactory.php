<?php

declare(strict_types=1);

namespace Core\Network;


/**
 * Class HttpFactory
 * @package Core\Network
 */
class HttpFactory implements RequestFactoryInterface, ResponseFactoryInterface
{
    public const DI = 'HttpFactory';

    /**
     * @param string $method
     * @param string $uri
     * @param string $requestParams
     * @return RequestInterface
     */
    public function createRequest(
        string $method = '',
        string $uri = '',
        string $requestParams = ''
    ): RequestInterface
    {
        $request = new Request(
            $method ?: $_SERVER['REQUEST_METHOD'],
            $uri ?: $_SERVER['REQUEST_URI'],
            $requestParams ?: $_SERVER['QUERY_STRING']
        );

        $request->setAcceptContent($_SERVER['HTTP_ACCEPT']);
        $request->setContentType($_SERVER['CONTENT_TYPE']);
        $request->setContentLength($_SERVER['CONTENT_LENGTH']);

        // all except multipart/form-data
        $request->setContent(file_get_contents('php://input'));

        return $request;
    }

    /**
     * @param int $code
     * @return ResponseInterface
     */
    public function createResponse(int $code = 200): ResponseInterface
    {
        return new Response($code);
    }
}

<?php

declare(strict_types=1);

namespace Core\Error;


class Error
{
    protected $message = '';

    protected $stack = [];

    /**
     * Error constructor.
     * @param string $message
     * @param array $stack
     */
    public function __construct(string $message, array $stack)
    {
        $this->message = $message;
        $this->stack = $stack;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getStack(): array
    {
        return $this->stack;
    }

    /**
     * @param array $stack
     */
    public function setStack(array $stack): void
    {
        $this->stack = $stack;
    }
}

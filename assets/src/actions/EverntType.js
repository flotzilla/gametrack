import * as Constants from './../constants/constants'

import axios from 'axios';

const EVENT_PREFIX = 'event-type/';

export const STATUS_OK = 'ok';
export const STATUS_ERROR = 'error';

const responseTemplate = {
    status: STATUS_OK,
    data: []
};

export function getEventTypes() {
    let r = {};
    Object.assign(r, responseTemplate);
    return axios.get(Constants.API_SERVER + EVENT_PREFIX)
        .then(response => {
            r.data = response.data.data;
        })
        .catch(e => {
            r.status = STATUS_ERROR;
            r.data = e;
        })
        .then( () => {
            return r;
        });
}

<?php

declare(strict_types=1);

namespace Core\Service;


use Core\Config\Config;
use Core\DataSource\Exception\WrongConfigException;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

/**
 * Class Logger
 * Simple logger example
 * @package Core\Service
 */
class Logger implements LoggerInterface
{
    use LoggerTrait;

    public const DI = 'Logger';

    /**
     * @var array
     */
    private $config = [];

    /** @var string $logDir */
    private $logDir = '';

    /** @var string $logFile */
    private $logFile = '';

    /**
     * Logger constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->loadConfig($config);
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     *
     * @return void
     *
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function log($level, $message, array $context = array())
    {
        $logMessage = '[' . date("Y.j.m-h:i:s") . '][' . $level . ']['
            . $_SERVER['REMOTE_ADDR'] . '][' . $message . '][' . json_encode($context) . ']' . PHP_EOL;
        $this->appendLog($logMessage);
    }

    /**
     * @param $log
     */
    private function appendLog($log)
    {
        file_put_contents(
            $this->logDir . date("j.n.Y") . '.' . $this->logFile,
            $log,
            FILE_APPEND
        );
    }

    /**
     * @param Config $config
     */
    private function loadConfig(Config $config)
    {
        $this->config = $config->getLoggerConfig();

        if (array_key_exists('logdir', $this->config)) {
            $this->logDir = $this->config['logdir'];
            $this->logFile = $config->getEnv() . '.log';
        } else {
            throw new WrongConfigException('Configuration cannot be loaded');
        }
    }
}

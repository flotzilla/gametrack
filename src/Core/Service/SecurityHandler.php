<?php

declare(strict_types=1);


namespace Core\Service;


use Core\Config\Config;
use Core\Middleware\MiddlewareInterface;
use Core\Network\RequestInterface;
use Core\Network\ResponseInterface;

/**
 * Class SecurityHandler
 * @package Core\Service
 */
class SecurityHandler implements MiddlewareInterface
{
    public const DI = 'SecurityHandler';

    /** @var Config $config */
    protected $config;

    /**
     * SecurityHandler constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }


    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function process(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $securityConf = $this->config->getSecurityConfig();
        if (array_key_exists('cors', $securityConf)) {
            foreach ($securityConf['cors'] as $corsItem) {
                $response->addHeader($corsItem);
            }
        }

        return $response;
    }
}

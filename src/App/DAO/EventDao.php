<?php

declare(strict_types=1);

namespace App\DAO;


use App\Model\Event;

/**
 * Interface EventDao
 * @package App\DAO
 */
interface EventDao
{
    /**
     * @param Event $event
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     * @throws \MongoDB\Driver\Exception\RuntimeException
     * @throws \MongoDB\Exception\BadMethodCallException
     * @throws \MongoDB\Exception\InvalidArgumentException
     * @return bool
     */
    public function insertEvent(Event $event): bool;


    /**
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     * @throws \MongoDB\Driver\Exception\RuntimeException
     * @throws \MongoDB\Exception\InvalidArgumentException
     * @return array
     */
    public function selectAll(): array;

    /**
     * @param string $id
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     * @throws \MongoDB\Driver\Exception\RuntimeException
     * @throws \MongoDB\Exception\InvalidArgumentException
     * @throws \MongoDB\Exception\UnsupportedException
     * @return Event|null
     */
    public function selectEventById(string $id): ?Event;
}

<?php

declare(strict_types=1);


namespace Core\Service;


use Core\Error\Error;
use Core\Middleware\MiddlewareInterface;
use Core\Network\RequestInterface;
use Core\Network\ResponseInterface;
use Core\Template\Template;
use Core\Util\Util;

class ErrorHandler implements MiddlewareInterface
{
    public const DI = 'ErrorHandler';

    /** @var Logger $logger */
    protected $logger;

    /** @var Template $template */
    protected $template;

    /**
     * ErrorHandler constructor.
     * @param Logger $logger
     * @param Template $template
     */
    public function __construct(Logger $logger, Template $template)
    {
        $this->logger = $logger;
        $this->template = $template;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function process(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        /** @var Error $error */
        if ($response->getStatusCode() === 200) {
            return $response;
        }

        foreach ($response->getErrors() as $error) {
            $this->logger->error($error->getMessage(), $error->getStack());
        }

        // if response already exists
        if ($response->getBody()
            && Util::isElementThatStartsWithStrInArray('Content-type', $response->getHeaders())) {
            return $response;
        }

        $response->setBody($this->defaultErrorResponseHandler($request, $response));

        return $response;
    }

    /**
     *
     * Dont judge me too much
     *
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return int|string
     */
    protected function defaultErrorResponseHandler(RequestInterface $request, ResponseInterface $response)
    {
        $templateName = $template = $response->getStatusCode();

        try {
            if (Util::isElementThatStartsWithStrInArray('Content-type: application/json', $response->getHeaders())) {

                $templateName =
                    ($this->template->isTemplateExists(strval($response->getStatusCode() . '.json'))
                        ? strval($response->getStatusCode())
                        : ResponseInterface::STATUS_500
                    )
                    . '.json';
                $template = $this->template->loadFile($templateName);
            } else {
                $templateName .= '.php';
                if ($this->template->isTemplateExists($templateName)) {
                    $template = $this->template
                        ->render('base.php', [
                            'title' => $response->getStatusCode() . ' Error',
                            'content' => $this->template->render($templateName)
                        ]);
                } else {
                    $template = $this->template
                        ->render('base.php', [
                            'title' => ResponseInterface::STATUS_500 . ' Error'
                        ]);
                }
            }

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        return $template;
    }
}

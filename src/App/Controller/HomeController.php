<?php

declare(strict_types=1);

namespace App\Controller;


use Core\Controller\Http\Controller;
use Core\Network\Router;

class HomeController extends Controller
{

    public function indexAction()
    {
        /** @var Router $router */
        $router = $this->container->get(Router::DI);
        $router->redirect('/dashboard');
    }
}

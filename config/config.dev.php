<?php

return [
    'router' => [
        'routes' => [
            '/' => [
                'controller' => 'App\\Controller\\Home',
                'action' => 'index',
                'allowedMethods' => ['GET']
            ],
            '/dashboard' => [
                'controller' => 'App\\Controller\\DashBoard',
                'action' => 'index'
            ],

            '/api/v1/event' => [
                'controller' => 'App\\Controller\\API\\V1\\Event\\Event',
            ],
            '/api/v1/event/:w' => [
                'controller' => 'App\\Controller\\API\\V1\\Event\\Event',
            ],
            '/api/v1/event-type' => [
                'controller' => 'App\\Controller\\API\\V1\\Event\\EventType',
            ]
            ,
            '/api/v1/event-type/:w' => [
                'controller' => 'App\\Controller\\API\\V1\\Event\\EventType',
            ]
        ]
    ],
    'templates' => [
        'directory' => dirname(__DIR__) . DIRECTORY_SEPARATOR . 'template'
    ],
    'db' => [
        'mongodb' => [
            'url' => 'mongodb://root:pass@mongodb:27017/gametrackdb'
        ]
    ],
    'logger' => [
        'logdir' => dirname(__DIR__) . '/var/log/',
        'level' => 'info'
    ],
    'security' => [
        'cors' => [
            'Access-Control-Allow-Origin: http://127.0.0.1',
        ]
    ]
];

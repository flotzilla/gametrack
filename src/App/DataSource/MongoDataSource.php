<?php

declare(strict_types=1);

namespace App\DataSource;

use Core\Config\Config;
use Core\DataSource\DataSource;
use Core\DataSource\Exception\WrongConfigException;
use MongoDB\Client;

class MongoDataSource implements DataSource
{
    public const DI = 'MongoDataSource';

    /** @var Client $connection */
    private $connection = null;

    private $connectUrl = '';

    private $config = [];

    /**
     * MongoDataSource constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->loadConfig($config);
    }

    /**
     * @inheritdoc
     */
    public function getConnection(): Client
    {
        if ($this->connection === null){
            $this->connection = new Client($this->connectUrl);
        }

        return $this->connection;
    }

    /**
     * @inheritdoc
     */
    public function loadConfig(Config $config)
    {
        $this->config = $config->getDBConfig();

        if (array_key_exists('mongodb', $this->config)
            && array_key_exists('url', $this->config['mongodb'])){
            $this->connectUrl = $this->config['mongodb']['url'];
        } else {
            throw new WrongConfigException('Configuration cannot be loaded');
        }
    }
}

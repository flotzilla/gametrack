<?php
declare(strict_types=1);

use App\DAO\EventDaoMongo;
use App\DAO\EventTypeDaoMongo;
use App\DataSource\MongoDataSource;
use App\Model\Event;
use App\Model\EventType;
use MongoDB\BSON\UTCDateTime;

/** @var \Core\Container\Container $container */
$container = require 'bootstrap.php';

/** @var MongoDataSource $mongoDataSource */
$mongoDataSource = $container->get(MongoDataSource::DI);

try {
    $conn = $mongoDataSource->getConnection();

    $conn->gametrackdb->createCollection('event');
    $conn->gametrackdb->createCollection('eventType');

    echo 'Successfully create db structure' . PHP_EOL;

    $eventTypes = [
        new EventType('Track in-game time', 'Time-tracking your game time'),
        new EventType('New subscription', 'New service subscription'),
        new EventType('Unsubscription', 'I cant get it anymore'),
        new EventType('Playing for 20 hours a day', 'A hero. True hero'),
        new EventType('Go blind', 'I can\'t see my screen'),
        new EventType('Gift', 'Oh my'),
        new EventType('Donation', 'Here comes the money'),
        new EventType('Abuse', 'Woman yelling at a cat'),
    ];

    $events = [
        (new Event())->setEventTypeId($eventTypes[0]->getId())
            ->setAcquisitionSource('test')
            ->setComment('comment')
            ->setData(['data' => 'val'])
            ->setDevice('generic andriod')
            ->setUtdDataTime(new UTCDateTime(DateTime::createFromFormat('Y-m-d H:i:s', '2020-02-10 01:55:25'))),
        (new Event())->setEventTypeId($eventTypes[1]->getId())
            ->setAcquisitionSource('test')
            ->setComment('comment')
            ->setData(['data' => 'val'])
            ->setDevice('Nexus 6')
            ->setUtdDataTime(new UTCDateTime(DateTime::createFromFormat('Y-m-d H:i:s', '2020-02-01 15:55:25'))),
        (new Event())->setEventTypeId($eventTypes[2]->getId())
            ->setAcquisitionSource('test')
            ->setComment('comment')
            ->setData(['data' => 'val'])
            ->setDevice('iphone 6')
            ->setUtdDataTime(new UTCDateTime(DateTime::createFromFormat('Y-m-d H:i:s', '2020-01-11 14:55:25'))),
        (new Event())->setEventTypeId($eventTypes[0]->getId())
            ->setAcquisitionSource('test')
            ->setComment('comment')
            ->setData(['data' => 'val'])
            ->setDevice('iphone 6')
            ->setUtdDataTime(new UTCDateTime(DateTime::createFromFormat('Y-m-d H:i:s', '2020-02-09 14:55:25'))),
        (new Event())->setEventTypeId($eventTypes[4]->getId())
            ->setAcquisitionSource('test')
            ->setComment('comment')
            ->setData(['data' => 'val'])
            ->setDevice('iphone X')
            ->setUtdDataTime(new UTCDateTime(DateTime::createFromFormat('Y-m-d H:i:s', '2019-11-09 14:55:25')))
    ];

    /** @var \App\DAO\EvenTypeDao $daoType */
    $daoType = $container->get(EventTypeDaoMongo::DI);
    /** @var EventDaoMongo $eventDaoType */
    $eventDaoType = $container->get(EventDaoMongo::DI);

    foreach ($eventTypes as $eventType) {
        $daoType->insertEventType($eventType);
    }

    foreach ($events as $ev){
        $eventDaoType->insertEvent($ev);
    }

    echo 'Successfully add new data' . PHP_EOL;

} catch (Exception $exception) {
    echo "Something goes wrong" . PHP_EOL;
    var_dump($exception->getMessage());
}

echo 'DONE' . PHP_EOL;


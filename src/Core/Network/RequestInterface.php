<?php

declare(strict_types=1);


namespace Core\Network;


interface RequestInterface
{

    public function getUrl(): string;

    public function getQueryParams(): string;

    public function getRequestMethod(): string;

    public function toArray(): array;

    public function getAcceptContent(): string;

    public function getContentType(): string;

    public function setAcceptContent(string $acceptContent);

    public function setContentType(string $contentType);

    public function setContentLength(string $contentLength);

    public function getContentLength(): string;

    public function setContent(string $contentLength);

    public function getContent(): string;
}

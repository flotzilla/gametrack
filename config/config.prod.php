<?php

return [
    'router' => [
        'routes' => [
            '/' => [
                'controller' => 'App\\Controller\\Home',
                'action' => 'index',
                'allowedMethods' => ['GET']
            ],
            '/dashboard' => [
                'controller' => 'App\\Controller\\DashBoard',
                'action' => 'index'
            ],
            '/dashboard/:id' => [
                'controller' => 'App\\Controller\\DashBoard',
                'action' => 'view'
            ]
        ]
    ],
    'templates' => [
        'directory' => dirname(__DIR__) . DIRECTORY_SEPARATOR . 'template'
    ],
    'db' => [
        'mongodb' => [
            'url' => 'mongodb://root:pass@mongodb:27017/gametrackdb'
        ]
    ],
    'logger' => [
        'logdir' => dirname(__DIR__) . '/var/log/',
        'level' => 'info'
    ]
];

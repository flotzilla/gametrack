import moment from "moment";

export const DATE_FILTER_TODAY = 'today';
export const DATE_FILTER_WEEK = 'week';
export const DATE_FILTER_MONTH = 'month';

let today = moment().startOf('day').utc();
let week = moment().startOf('isoWeek').utc();
let month = moment().startOf('month').utc();

function filter(data, filter) {
    let result = [];
    data.forEach(el => {
        if (moment(el['timestamp']['date']).utc().isAfter(filter)) {
            result.push(el);
        }
    });

    return result;
}

export function filterByDate(data = [], dateFilter) {
    let result = [];
    switch (dateFilter) {
        case DATE_FILTER_TODAY:
            result = filter(data, today);
            break;
        case DATE_FILTER_WEEK:
            result = filter(data, week);
            break;
        case DATE_FILTER_MONTH:
            result = filter(data, month);
            break;
        default:
            break;
    }
    return result;
}

export function filterByType(events = [], eventTypes = []) {
    let filteredEvents = new Map();
    eventTypes.forEach(ev => {
        filteredEvents.set(ev['_id'], {name: ev['name'], data: []});
    });

    events.forEach(ev => {
        if (filteredEvents.has(ev['eventType'])) {
            filteredEvents.get(ev['eventType']).data.push(ev);
        }
    });

    let res = [];
    filteredEvents.forEach((val) => {
        res.push(
            {
                name: val.name,
                y: val.data.length
            });
    });
    return res;
}

export function filterByDevice(events = []) {
    let elements = new Map();
    events.forEach(el => {
        if (elements.has(el['device'])) {
            elements.get(el['device']).data += 1;
        } else {
            elements.set(el['device'], {data: 1})
        }
    });

    return Array.from(elements, (k) => { return { name: k[0], y: k[1].data} });
}
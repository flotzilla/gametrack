<?php
declare(strict_types=1);


namespace Core\Sanitizer;


/**
 * Interface Sanitizer
 * @package Core\Sanitizer
 */
interface Sanitizer
{
    /**
     * @param array $array
     * @return array
     */
    public function sanitize(array $array): array;
}

import 'babel-polyfill';

import Vue from 'vue'
import App from './App.vue'

import router from './src/router/index'

// Vue.config.devtools = true;
Vue.config.productionTip = false;

const app = new Vue(
    {
        el: '#app',
        router: router,
        render: h => h(App)
    }
);

console.log('well hello');

<?php

declare(strict_types=1);

namespace App\Service;


use App\DAO\EventDao;
use App\Model\Event;
use Core\Service\Logger;
use Psr\Container\ContainerInterface;

/**
 * Class EventService
 * @package App\Service
 */
class EventService
{
    public const DI = 'EventService';

    /** @var EventDao $eventDao */
    protected $eventDao;

    /** @var ContainerInterface $container */
    private $container;

    /**
     * EventService constructor.
     * @param EventDao $eventDao
     * @param ContainerInterface $container
     */
    public function __construct(EventDao $eventDao, ContainerInterface $container)
    {
        $this->eventDao = $eventDao;
        $this->container = $container;
    }

    /**
     * @param Event $event
     * @return bool
     */
    public function addEvent(Event $event): bool
    {
        $isSuccess = false;
        try {
            $isSuccess = $this->eventDao->insertEvent($event);
        } catch (\Exception $e) {
            $logger = $this->container->get(Logger::DI);
            $logger->error($e->getMessage(), $e->getTrace());
        }

        return $isSuccess;
    }

    /**
     * @return array
     */
    public function selectAll(): array
    {
        $events = [];
        try {
            $events = $this->eventDao->selectAll();
        } catch (\Exception $e) {
            /** @var Logger $logger */
            $logger = $this->container->get(Logger::DI);
            $logger->error($e->getMessage(), $e->getTrace());
        }

        return $events;
    }

    /**
     * @param string $id
     * @return Event|null
     */
    public function selectEventById(string $id): ?Event
    {
        $event = null;

        try {
            $event = $this->eventDao->selectEventById($id);
        } catch (\Exception $e) {
            /** @var Logger $logger */
            $logger = $this->container->get(Logger::DI);
            $logger->error($e->getMessage(), $e->getTrace());
        }

        return $event;
    }
}

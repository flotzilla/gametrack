<?php

namespace App\Sanitizer;


use Core\Sanitizer\Sanitizer;
use MongoDB\BSON\ObjectId;

/**
 * Class EventSanitizer
 * @package App\Sanitizer
 */
class EventSanitizer implements Sanitizer
{
    /**
     * @param array $array
     * @return array
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     */
    public function sanitize(array $array): array
    {
        //TODO, clean every field here
        $array['eventType'] = new ObjectId($array['eventType']);
        return $array;
    }
}

<?php

declare(strict_types=1);

namespace Core\Controller\Http;


use Core\Controller\Controller as BaseController;
use Core\Network\RequestInterface;
use Core\Network\ResponseInterface;
use Core\Template\Template;
use Psr\Container\ContainerInterface;

class Controller extends BaseController
{
    /** @var Template */
    protected $template;

    public function __construct(
        RequestInterface $request,
        ResponseInterface $response,
        ContainerInterface $container
    )
    {
        parent::__construct($request, $response, $container);
        $this->template = $container->get(Template::DI);
    }

    /**
     * @return Template
     */
    public function getTemplate(): Template
    {
        return $this->template;
    }

    /**
     * @param Template $template
     */
    public function setTemplate(Template $template): void
    {
        $this->template = $template;
    }

    /**
     * @return void
     */
    protected function after(): void
    {
        $this->response->addHeader('Content-type: text/html; charset=UTF-8');
    }

    protected function before(): void
    {
        // do nothing
    }
}

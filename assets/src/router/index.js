import VueRouter from 'vue-router'
import About from '../pages/About.vue'
import Dashboard from '../pages/Dashboard.vue'
import Events from '../pages/Events.vue'
import NotFound from '../pages/NotFoundPage.vue'
import Vue from 'vue'

Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        {path: '/', component: Dashboard},
        {path: '/events', component: Events},
        {path: '/about', component: About},
        {path: '*', component: NotFound}
    ]
});

export default router

<?php

declare(strict_types=1);


namespace Core\Util;


class Util
{
    public static function isElementThatStartsWithStrInArray(string $needle, array $array, bool $sameCase = true): bool
    {
        $result = array_filter($array, function ($key) use ($sameCase, $needle) {
            if ($sameCase) {
                return strpos($key, $needle) === 0;
            }
            return strpos(strtolower($key), strtolower($needle)) === 0;
        });

        return count($result) > 0;
    }
}

<?php

declare(strict_types=1);

namespace Core\Exception;


use RuntimeException;

class ControllerNotFoundException extends RuntimeException
{
    protected $message = 'Handling controller is not found';
}

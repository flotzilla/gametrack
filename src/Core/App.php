<?php

declare(strict_types=1);

namespace Core;


use Core\Middleware\MiddlewareInterface;
use Core\Network\RequestInterface;
use Core\Network\ResponseInterface;

class App
{
    /** @var MiddlewareInterface[] */
    private $queue = [];

    /** @var RequestInterface */
    private $request;

    /** @var ResponseInterface */
    private $response;

    /**
     * App constructor.
     * @param MiddlewareInterface[] $stack
     */
    public function __construct(array $stack)
    {
        $this->queue = $stack;
    }

    /**
     * @param MiddlewareInterface $middleware
     */
    public function add(MiddlewareInterface $middleware): void
    {
        $this->queue[] = $middleware;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function run(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $this->request = $request;
        $this->response = $response;

        $stackSize = count($this->queue);

        $stackCounter = 0;

        while ($stackCounter < $stackSize) {
            $this->response = $this->queue[$stackCounter]->process($this->request, $this->response);
            $stackCounter++;
        }

        return $this->response;
    }
}

<?php

declare(strict_types=1);

namespace Core\Network;


use Core\Error\Error;

interface ResponseInterface
{
    public const STATUS_200 = 200;
    public const STATUS_404 = 404;
    public const STATUS_405 = 405;
    public const STATUS_500 = 500;

    public function setHeaders(array $h): void;

    public function getHeaders(): array;

    public function addHeader(string $header): void;

    public function setBody(string $body): void;

    public function getBody(): string ;

    public function setStatusCode(int $statusCode): void;

    public function getStatusCode(): int;

    public function getContentType(): string;

    public function setContentType(string $contentType): void;

    public function setErrors(array $errors): void;

    public function getErrors(): array;

    public function addError(Error $error): void;

    public function getParsedRequestParams(): array;

    public function setParsedRequestParams(array $params): void;

    public function getParsedUrlParam(): string ;

    public function setParsedUrlParam(string $param): void;

}

<?php

declare(strict_types=1);

namespace Core\Container\Exception;


use Psr\Container\NotFoundExceptionInterface;

class ContainerNotFoundException extends \InvalidArgumentException implements NotFoundExceptionInterface
{
    protected $message = "DI Service not found";

}

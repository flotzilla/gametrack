<?php
declare(strict_types=1);

use Core\App;
use Core\Container\Container;
use Core\Network\HttpFactory;
use Core\Network\Router;
use Core\Service\ContentHandler;
use Core\Service\ControllerHandler;
use Core\Service\ErrorHandler;
use Core\Service\SecurityHandler;

/** @var Container $container */
$container = require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'bootstrap.php';

$app = new App([
    $container->get(SecurityHandler::DI),
    $container->get(Router::DI),
    $container->get(ControllerHandler::DI),
    $container->get(ErrorHandler::DI),
    $container->get(ContentHandler::DI),
]);

/** @var HttpFactory $httpFactory */
$httpFactory = $container->get(HttpFactory::DI);

$app->run(
    $httpFactory->createRequest(), $httpFactory->createResponse()
);

<?php

declare(strict_types=1);

namespace Core\Network;


use Core\Config\Config;
use Core\Error\Error;
use Core\Middleware\MiddlewareInterface;
use Core\Service\Logger;
use Psr\Container\ContainerInterface;

/**
 * Class Router
 * @package Core\Network
 */
class Router implements MiddlewareInterface
{
    public const DI = 'Router';

    /** @var Config $config */
    protected $config;

    /** @var array $routes */
    protected $routes;

    /** @var ContainerInterface $container */
    protected $container;

    /** @var Logger $logger */
    protected $logger;

    /** @var array $replaceRoutes default replace rules */
    protected $replaceRoutes = [
        '/\//' => '\\/'
    ];
    /** @var array $urlIdRoutes routes for handling id in url like /url/123 */
    protected $urlIdRoutes = [
        '/\:id/' => '\/?\d+',
        '/\:w/' => '([a-zA-z0-9]*)',
    ];

    /**
     * Router constructor.
     * @param Config $config
     * @param ContainerInterface $container
     */
    public function __construct(Config $config, ContainerInterface $container)
    {
        $this->config = $config;
        $this->container = $container;

        // init router from config
        $router = $config->getRouter();
        if (array_key_exists('routes', $router)) {
            foreach ($router['routes'] as $routeKey => $routeVal) {
                $this->add($routeKey, $routeVal);
            }
        }

        $this->logger = $this->container->get(Logger::DI);
    }

    /**
     * Add new route to router
     *
     * @param string $route
     * @param array $params
     */
    public function add(string $route, array $params = []): void
    {
        $this->routes[$this->filter($route)] = $params;
    }

    /**
     * @param string $route
     * @return string
     */
    public function filter(string $route): string
    {
        foreach ($this->replaceRoutes as $replaceRouteK => $replaceRouteV) {
            $route = preg_replace($replaceRouteK, $replaceRouteV, $route);
        }

        foreach ($this->urlIdRoutes as $idRouteK => $idRouteV) {
            $route = preg_replace($idRouteK, $idRouteV, $route);
        }

        return '/^' . $route . '$/i';
    }

    /**
     * @param string $url
     * @return array
     */
    public function getRouteParamsByUrl(string $url): array
    {
        $url = $this->cleanUpUrl($url);

        $response = [];

        foreach ($this->routes as $route => $params) {

            if (preg_match($route, $url, $matches)) {
                foreach ($matches as $key => $match) {
                    if (is_string($key)) {
                        $params[$key] = $match;
                    }
                }

                $response = $params;
            }
        }

        return $response;
    }

    /**
     * Remove unnecessary parameters from url string
     * @param string $url
     * @return string
     */
    protected function cleanUpUrl(string $url): string
    {
        if ($pos = strpos($url, '?')) {
            $url = substr($url, 0, strpos($url, '?'));
        }

        // slash at the end
        if (strlen($url) > 1) {
            $url = preg_replace('{/$}', '', $url);
        }

        return $url;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function process(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface
    {
        if ($response->getStatusCode() !== ResponseInterface::STATUS_200) {
            return $response;
        }

        $requestDebug = $request->toArray();
        $this->logger->info($request->getRequestMethod(), $requestDebug);

        try {
            $url = $this->cleanUpUrl($request->getUrl());

            if ($matchedRoute = $this->match($url)) {
                $response->setStatusCode(ResponseInterface::STATUS_200);
                $response->setParsedRequestParams($this->parseHttpParams($request->getQueryParams()));
                $response->setParsedUrlParam($this->parseCollectionParamsFromUrl($url, $matchedRoute));
            } else {
                $response->setStatusCode(ResponseInterface::STATUS_404);
                $response->addError(
                    new Error("No route matched", $requestDebug));
            }

        } catch (\Exception $e) {
            /** @var HttpFactory $httpFactory */
            $httpFactory = $this->container->get(HttpFactory::DI);
            $response = $httpFactory->createResponse(ResponseInterface::STATUS_500);
            $response->addError(new Error($e->getMessage(), $e->getTrace()));
        }

        return $response;
    }

    /**
     * Match url by defined patterns
     *
     * @param string $url
     * @return string
     */
    public function match(string $url): string
    {
        $matchedRoute = '';
        foreach ($this->routes as $route => $params) {

            if (preg_match($route, $url, $matches)) {
                $matchedRoute = $route;
                break;
            }
        }

        return $matchedRoute;
    }

    /**
     * Split request parameters into array
     *
     * @param string $params
     * @return array
     */
    protected function parseHttpParams(string $params): array
    {
        $parameters = [];

        if ($params != '') {
            $re = '/((([a-zA-Z0-9]{1,})=([a-zA-Z0-9]{0,}))*)|([a-zA-Z0-9]+[=]{0,})/m';
            preg_match_all($re, $params, $response, PREG_SET_ORDER, 0);

            foreach ($response as $r) {
                if ($r[3] != '') {
                    $parameters[$r[3]] = $r[4];
                }
            }
        }

        return $parameters;
    }

    /**
     * @param string $url
     * @param string $matchedRoute
     * @return string
     */
    protected function parseCollectionParamsFromUrl(string $url, string $matchedRoute): string
    {
        $response = '';
        foreach ($this->urlIdRoutes as $idRoute) {
            if (strpos($matchedRoute, $idRoute) !== false) {
                preg_match('/([^\/]+$)/', $url, $matches);
                if (count($matches) > 0) {
                    $response = $matches[0];
                }
            }
        }
        return $response;
    }

    /**
     * @param string $route
     */
    public function redirect(string $route = '/')
    {
        header("Location: " . $route);
    }

    /**
     * @return array
     */
    public function getReplaceRoutes(): array
    {
        return $this->replaceRoutes;
    }

    /**
     * @param array $replaceRoutes
     */
    public function setReplaceRoutes(array $replaceRoutes): void
    {
        $this->replaceRoutes = $replaceRoutes;
    }

    /**
     * @return array
     */
    public function getUrlIdRoutes(): array
    {
        return $this->urlIdRoutes;
    }

    /**
     * @param array $urlIdRoutes
     */
    public function setUrlIdRoutes(array $urlIdRoutes): void
    {
        $this->urlIdRoutes = $urlIdRoutes;
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }
}

const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
    mode: 'development',
    entry: './assets/main.js',
    output: {
        path: path.resolve(__dirname, "./public/assets"),
        publicPath: '/assets/',
        filename: 'bundle.js',
        // contentBase:
    },
    resolve: {
        // extensions: ['.js', '.vue'],
        // alias: {
        //     components: path.join(__dirname, './assets')
        // }
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        scss: ['vue-style-loader', 'css-loader', 'sass-loader']
                    }
                }
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.css$/i,
                use: [
                    'css-loader',
                ],
            },
        ]
    },

    plugins: [
        new VueLoaderPlugin()
    ],
    devServer: {
        historyApiFallback: true,
        contentBase: path.join(__dirname, './public'),
        overlay: true,
    },
    devtool: 'inline-source-map',
    performance: {
        hints: false
    }
};
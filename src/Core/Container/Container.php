<?php

declare(strict_types=1);

namespace Core\Container;


use Core\Container\Exception\ContainerNotFoundException;
use Psr\Container\ContainerInterface;

class Container implements ContainerInterface
{
    /** @var array  */
    private $services = [];

    /** @var \Closure[] */
    private $serviceFactories = [];

    /**
     * Container constructor.
     * @param array $serviceFactories
     */
    public function __construct(array $serviceFactories = [])
    {
        foreach ($serviceFactories as $id => $serviceFactory){
            $this->set($id, $serviceFactory);
        }
    }

    /**
     * @inheritdoc
     */
    public function get($id)
    {
        if (!$this->has($id)){
            throw new ContainerNotFoundException($id);
        }
        $service = $this->services[$id];

        if(!isset($service)){
            $service = $this->getFromFactory($id);
        }

        return $service;
    }

    /**
     * @param string $id
     * @param \Closure $serviceFactory
     */
    public function set(string $id, \Closure $serviceFactory){
        $this->serviceFactories[$id] = $serviceFactory;

        // remove previous
        unset($this->services[$id]);
    }

    /**
     * @inheritdoc
     */
    public function has($id): bool
    {
        return isset($this->serviceFactories[$id]);
    }

    /**
     * @return array
     */
    public function listServiceIds(): array {
        return array_keys($this->serviceFactories);
    }

    /**
     * @param string $id
     * @return mixed
     */
    private function getFromFactory(string $id)
    {
        $serviceFactory = $this->serviceFactories[$id];
        return $serviceFactory($this);
    }
}

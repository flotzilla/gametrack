<?php

namespace Core\Service;


use Core\Middleware\MiddlewareInterface;
use Core\Network\HttpFactory;
use Core\Network\RequestInterface;
use Core\Network\ResponseInterface;
use Psr\Container\ContainerInterface;

class ResponseHandler implements MiddlewareInterface
{
    public const DI = 'ResponseHandler';

    /** @var ContainerInterface $conteainer */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function process(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        /** @var HttpFactory $httpFactory */
        $httpFactory = $this->container->get(HttpFactory::DI);
        return $httpFactory->createResponse();
    }
}

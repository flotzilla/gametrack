## Gametrack

#### Preface
 
Original idea was to implement all PSR php-fig [standards](https://www.php-fig.org/psr/) recommendations.
I started this task, but noticed that this project requires a lot of time and effort from me. 
It would be a crazy idea to write modern php application without any additional libraries or partial implementations in a few days.
So, I decide, that implementing of all this recommendations on my own would be a little bit overhead. 
I guess, that for small skill demonstration its a good shot.  
Eventually I choose to write simple(almost) MVC-base applications with partial implementation of PSR recommendations.

### Preparations

* run `docker-compose up -d` inside project directory
* run `docker-compose run php-cli zsh` to get access to shell
* inside shell run `composer install` and them `php initDB.php`
* run `yarn install && yarn build`

* open [127.0.0.1](http://127.0.0.1)


Eventually project is not finished, routes are working, some of DAO controller in use.
Frontend part is slightly done.

For creating new events try to send post request to 127.0.0.1/api/v1/event
with parameters like this:
```
{
	"eventType": "created_event_type_id",
	"device": "iphone 6",
	"acquisitionSource": "very trusted source",
	"comment": "well hello there",
	"data": {
		"somef": "someV"
	}
}
```


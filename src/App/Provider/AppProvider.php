<?php

declare(strict_types=1);

namespace App\Provider;


use App\DAO\EventTypeDaoMongo;
use App\Service\EventService;
use App\DAO\EventDaoMongo;
use App\DataSource\MongoDataSource;
use App\Service\EventTypeService;
use Core\Config\Config;
use Core\Container\Container;
use Core\Network\HttpFactory;
use Core\Network\Router;
use Core\Service\ContentHandler;
use Core\Service\ControllerHandler;
use Core\Service\ErrorHandler;
use Core\Service\Logger;
use Core\Service\ResponseHandler;
use Core\Service\SecurityHandler;
use Core\Service\ServiceProviderInterface;
use Core\Template\Template;
use Psr\Container\ContainerInterface;

/**
 * Class AppProvider
 * @package App\Provider
 */
class AppProvider implements ServiceProviderInterface
{

    public function register(Container $container)
    {
        $container->set(Router::DI, function (ContainerInterface $container) {
            return new Router($container->get(Config::DI), $container);
        });

        $container->set(Template::DI, function () {
            return new Template(ROOT_CONFIG . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR );
        });

        $container->set(MongoDataSource::DI, function (ContainerInterface $container) {
            return new MongoDataSource($container->get(Config::DI));
        });

        $container->set(EventDaoMongo::DI, function (ContainerInterface $container) {
            return new EventDaoMongo($container->get(MongoDataSource::DI) );
        });

        $container->set(EventService::DI, function (ContainerInterface $container) {
            return new EventService($container->get(EventDaoMongo::DI), $container );
        });

        $container->set(EventTypeDaoMongo::DI, function (ContainerInterface $container) {
            return new EventTypeDaoMongo($container->get(MongoDataSource::DI) );
        });

        $container->set(EventTypeService::DI, function (ContainerInterface $container) {
            return new EventTypeService($container->get(EventTypeDaoMongo::DI), $container );
        });

        $container->set(Logger::DI, function (ContainerInterface $container) {
            return new Logger($container->get(Config::DI));
        });

        $container->set(ContentHandler::DI, function () {
            return new ContentHandler();
        });

        $container->set(ErrorHandler::DI, function (ContainerInterface $container) {
            return new ErrorHandler($container->get(Logger::DI), $container->get(Template::DI));
        });

        $container->set(ControllerHandler::DI, function (ContainerInterface $container) {
            return new ControllerHandler($container->get(Router::DI), $container);
        });

        $container->set(ResponseHandler::DI, function (ContainerInterface $container) {
            return new ResponseHandler($container);
        });

        $container->set(SecurityHandler::DI, function (ContainerInterface $container) {
            return new SecurityHandler($container->get(Config::DI));
        });

        $container->set(HttpFactory::DI, function () {
            return new HttpFactory();
        });
    }
}

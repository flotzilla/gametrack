<?php

declare(strict_types=1);


namespace App\Controller\API\V1\Event;


use App\Model\Event;
use App\Sanitizer\EventSanitizer;
use App\Service\EventService;
use App\Service\EventTypeService;
use App\Validator\EventValidator;
use Core\Controller\Rest\Controller as RestController;
use Core\Service\Logger;
use JsonException;
use MongoDB\BSON\ObjectId;

/**
 * Class EventController
 * @package App\Controller\API\V1\Event
 */
class EventController extends RestController
{

    public function listAction()
    {
        $response = [];

        /** @var EventService $eventService */
        $eventService = $this->container->get(EventService::DI);
        $events = $eventService->selectAll() ?: [];
        foreach ($events as $event) {
            $el = $event->bsonSerialize();
            $el['_id'] = $el['_id']->jsonSerialize()['$oid'];
            $el['eventType'] = $el['eventType']->jsonSerialize()['$oid'];
            $el['timestamp'] = $el['timestamp']->toDateTime();
            $response[] = $el;
        }

        return json_encode($response);
    }

    public function getAction()
    {
        $id = $this->response->getParsedUrlParam();

        // very basic check
        if (strlen($id) != 24) {
            return json_encode([
                'type' => 'error',
                'message' => 'Invalid id format'
            ]);
        }

        /** @var EventService $eventService */
        $eventService = $this->container->get(EventService::DI);

        $response = $eventService->selectEventById($id);

        if($response){
            $response = $response->bsonSerialize();
        }

        return json_encode($response?: []);
    }

    public function postAction()
    {
        /** @var Logger $logger */
        $logger = $this->container->get(Logger::DI);
        $response = ['status' => 'ok'];

        try {
            $payload = json_decode($this->request->getContent(),
                $assoc = true,
                $depth = 512,
                JSON_THROW_ON_ERROR);

            $validator = new EventValidator();

            if (!$validator->validate($payload)) {
                $response['status'] = 'error';
                $response['errors'] = $validator->getErrors();

                return json_encode($response);
            }

            $payload = (new EventSanitizer())->sanitize($payload);

            $event = new Event();
            $event->bsonUnserialize($payload);

            /** @var EventTypeService $eventTypeService */
            $eventTypeService = $this->container->get(EventTypeService::DI);
            if(!$event->getEventTypeId() || !($event->getEventTypeId() instanceof ObjectId)
                || !$eventTypeService->selectEventTypeById($event->getEventTypeId()->jsonSerialize()['$oid'])){
                $response['status'] = 'error';
                $response['errors'] = 'Wrong Event Type';

                return json_encode($response);
            }

            /** @var EventService $eventService */
            $eventService = $this->container->get(EventService::DI);
            if (!$eventService->addEvent($event)) {
                $response['status'] = 'error';
                $response['message'] = ['Sorry an error occurred'];
            }

        } catch (JsonException $e) {
            $logger->info('Invalid data format', ['payload' => $this->request->getContent()]);

            $response['status'] = 'error';
            $response['message'] = ['Invalid request data format'];
        } catch (\Throwable $exception) {
            $logger->error($exception->getMessage(), $exception->getTrace());

            $response['status'] = 'error';
            $response['message'] = ['Sorry an error occurred'];
        } finally {
            return json_encode($response);
        }
    }
}

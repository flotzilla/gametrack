<?php
declare(strict_types=1);

namespace App\Controller\API\V1\Event;


use App\Service\EventTypeService;
use Core\Controller\Rest\Controller as RestController;


/**
 * Class EventTypeController
 * @package App\Controller\API\V1\Event
 */
class EventTypeController extends RestController
{

    /**
     * @return false|string
     */
    public function listAction()
    {
        $response = [];

        /** @var EventTypeService $eventService */
        $eventService = $this->container->get(EventTypeService::DI);
        $eventTypes = $eventService->selectAll() ?: [];
        foreach ($eventTypes as $eventType) {
            $el = $eventType->bsonSerialize();
            $el['_id'] = $el['_id']->jsonSerialize()['$oid'];
            $response[] = $el;
        }

        return json_encode([
            'data' => $response
        ]);
    }

    public function getAction()
    {

    }

    // skip other methods
}
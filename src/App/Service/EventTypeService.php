<?php

declare(strict_types=1);

namespace App\Service;


use App\DAO\EvenTypeDao;
use App\Model\EventType;
use Core\Service\Logger;
use Psr\Container\ContainerInterface;

/**
 * Class EventTypeService
 * @package App\Service
 */
class EventTypeService
{
    public const DI = 'EventTypeService';

    /** @var EvenTypeDao $eventTypeDao */
    protected $evenTypeDao;

    /** @var ContainerInterface $container */
    private $container;

    /**
     * EventTypeService constructor.
     * @param EvenTypeDao $eventDao
     * @param ContainerInterface $container
     */
    public function __construct(EvenTypeDao $eventDao, ContainerInterface $container)
    {
        $this->evenTypeDao = $eventDao;
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function selectAll(): array
    {
        $eventTypes = [];

        try {
            $eventTypes = $this->evenTypeDao->selectAllEventTypes();
        } catch (\Exception $e) {
            $logger = $this->container->get(Logger::DI);
            $logger->error($e->getMessage(), $e->getTrace());
        }

        return $eventTypes;
    }

    /**
     * @param null|string $id
     * @return EventType|null
     */
    public function selectEventTypeById(?string $id): ?EventType
    {
        $event = null;

        if (!$id){
            return $event;
        }

        try {
            $event = $this->evenTypeDao->selectEventTypeById($id);
        } catch (\Exception $e) {
            /** @var Logger $logger */
            $logger = $this->container->get(Logger::DI);
            $logger->error($e->getMessage(), $e->getTrace());
        }

        return $event;
    }
}

<?php

declare(strict_types=1);

namespace Core\Service;


use Core\Middleware\MiddlewareInterface;
use Core\Network\RequestInterface;
use Core\Network\ResponseInterface;

/**
 * Class ContentHandler
 * @package Core\Service
 */
class ContentHandler implements MiddlewareInterface
{
    public const DI = 'ContentHandler';

    public function process(RequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        foreach ($response->getHeaders() as $header){
            header($header);
        }
        http_response_code($response->getStatusCode());

        echo $response->getBody();

        return $response;
    }
}

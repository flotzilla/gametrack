<?php

declare(strict_types=1);

namespace Core\Controller\Rest;


use Core\Controller\Controller as BaseController;
use Core\Exception\ControllerMethodNotFound;
use Core\Network\ResponseInterface;


/**
 * Class Controller
 * @package Core\Controller\Rest
 */
class Controller extends BaseController
{
    public const ALLOWED_METHODS_BY_DEFAULT = [
        'GET',
        'POST',
        'PUT',
        'PATCH',
        'DELETE',
    ];

    protected $allowActionRewrite = true;

    protected $defaultContentType = 'Content-type: application/json; charset=utf-8';

    /**
     * @inheritdoc
     */
    protected function after(): void
    {
        $this->response->addHeader('Content-type: application/json; charset=utf-8');
    }

    /**
     * Call controller action (defined as nameAction in controller) or call default class method
     *
     * @param $name
     * @param $arguments
     * @return ResponseInterface|mixed
     * @throws ControllerMethodNotFound
     */
    public function __call($name, $arguments)
    {
        if (empty($this->response->getParsedUrlParam()) && $name == 'get'){
            $name = 'list';
        }

        return parent::__call($name, $arguments);
    }


    /**
     * @return array
     */
    public function getAllowedMethods(): array
    {
        return $this->allowedMethods ?: self::ALLOWED_METHODS_BY_DEFAULT;
    }

    /**
     * @inheritdoc
     */
    protected function before(): void
    {
    }
}
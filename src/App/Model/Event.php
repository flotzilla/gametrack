<?php

declare(strict_types=1);

namespace App\Model;


use DateTime;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\Serializable;
use MongoDB\BSON\Unserializable;
use MongoDB\BSON\UTCDateTime;

/**
 * Class Event
 * @package App\Model
 */
class Event implements Serializable, Unserializable
{
    /** @var ObjectId $id */
    private $id;

    /** @var ObjectId $eventType */
    private $eventType = null;

    /** @var string $device */
    private $device = '';

    /** @var string $acquisitionSource */
    private $acquisitionSource;

    /** @var string $comment */
    private $comment = '';

    /** @var array */
    private $data = [];

    /** @var UTCDateTime $utdDataTime */
    private $utdDataTime;

    /**
     * Event constructor.
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     * @throws \Exception
     */
    public function __construct()
    {
        $this->id = new ObjectId;
        $this->utdDataTime = new UTCDateTime(new DateTime());
    }

    /**
     * @return ObjectId|null
     */
    public function getEventTypeId(): ?ObjectId
    {
        return $this->eventType;
    }

    /**
     * @param ObjectId $eventType
     * @return Event
     */
    public function setEventTypeId(ObjectId $eventType): Event
    {
        $this->eventType = $eventType;

        return $this;
    }

    /**
     * @return string
     */
    public function getDevice(): string
    {
        return $this->device;
    }

    /**
     * @param string $device
     * @return Event
     */
    public function setDevice(string $device): Event
    {
        $this->device = $device;

        return $this;
    }

    /**
     * @return string
     */
    public function getAcquisitionSource(): string
    {
        return $this->acquisitionSource;
    }

    /**
     * @param string $acquisitionSource
     * @return Event
     */
    public function setAcquisitionSource(string $acquisitionSource): Event
    {
        $this->acquisitionSource = $acquisitionSource;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return Event
     */
    public function setComment(string $comment): Event
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return Event
     */
    public function setData(array $data): Event
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return ObjectId
     */
    public function getId(): ObjectId
    {
        return $this->id;
    }

    /**
     * @return UTCDateTime
     */
    public function getUtdDataTime(): UTCDateTime
    {
        return $this->utdDataTime;
    }

    /**
     * @param UTCDateTime $utdDataTime
     * @return Event
     */
    public function setUtdDataTime(UTCDateTime $utdDataTime): Event
    {
        $this->utdDataTime = $utdDataTime;

        return $this;
    }

    /**
     * Provides an array or document to serialize as BSON
     * Called during serialization of the object to BSON. The method must return an array or stdClass.
     * Root documents (e.g. a MongoDB\BSON\Serializable passed to MongoDB\BSON\fromPHP()) will always be serialized as a BSON document.
     * For field values, associative arrays and stdClass instances will be serialized as a BSON document and sequential arrays (i.e. sequential, numeric indexes starting at 0) will be serialized as a BSON array.
     * @link https://php.net/manual/en/mongodb-bson-serializable.bsonserialize.php
     * @return array|object An array or stdClass to be serialized as a BSON array or document.
     */
    public function bsonSerialize()
    {
        return [
            '_id' => $this->id,
            'eventType' => $this->eventType,
            'device' => $this->device,
            'acquisitionSource' => $this->acquisitionSource,
            'comment' => $this->comment,
            'data' => $this->data,
            'timestamp' => $this->utdDataTime
        ];
    }

    /**
     * Constructs the object from a BSON array or document
     * Called during unserialization of the object from BSON.
     * The properties of the BSON array or document will be passed to the method as an array.
     * @link https://php.net/manual/en/mongodb-bson-unserializable.bsonunserialize.php
     * @param array $data Properties within the BSON array or document.
     */
    public function bsonUnserialize(array $data)
    {
        $this->id = $data['_id']?: $this->id;
        $this->eventType = $data['eventType'];
        $this->device = $data['device'];
        $this->acquisitionSource = $data['acquisitionSource'];
        $this->comment = $data['comment'] ?: '';
        $this->data = $data['data'] ?: [];
        $this->utdDataTime = $data['timestamp']?: $this->utdDataTime;
    }
}

<?php

declare(strict_types=1);


namespace Core\Exception;


class InvalidClassException extends \Exception
{
    protected $message = "Called class should extends \Core\Controller class";
}

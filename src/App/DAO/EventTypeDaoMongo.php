<?php

declare(strict_types=1);

namespace App\DAO;


use App\Model\EventType;
use Core\DataSource\DataSource;
use MongoDB\BSON\ObjectId;
use MongoDB\Collection;
use MongoDB\Model\BSONDocument;

/**
 * Class EventTypeDaoMongo
 * @package App\DAO
 */
class EventTypeDaoMongo implements EvenTypeDao
{
    public const DI = 'EventTypeDaoMongo';

    /** @var DataSource  */
    private $dataSource;

    /**
     * EventDaoMongo constructor.
     * @param DataSource $dataSource
     */
    public function __construct(DataSource $dataSource)
    {
        $this->dataSource = $dataSource;
    }

    /**
     * @param EventType $event
     * @return bool
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     * @throws \MongoDB\Driver\Exception\RuntimeException
     * @throws \MongoDB\Exception\BadMethodCallException
     * @throws \MongoDB\Exception\InvalidArgumentException
     */
    public function insertEventType(EventType $event): bool
    {
        $connection = $this->dataSource->getConnection();
        /** @var Collection $collection */
        $collection = $connection->gametrackdb->eventType;
        return $collection->insertOne($event)
                ->getInsertedCount() > 0;
    }

    /**
     * @return array
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     * @throws \MongoDB\Driver\Exception\RuntimeException
     * @throws \MongoDB\Exception\InvalidArgumentException
     * @throws \MongoDB\Exception\UnsupportedException
     */
    public function selectAllEventTypes(): array
    {
        $result = [];

        $connection = $this->dataSource->getConnection();
        /** @var Collection $collection */
        $collection = $connection->gametrackdb->eventType;
        /** @var BSONDocument $dbResponse */
        $dbResponse = $collection->find()->toArray() ?: [];
        foreach ($dbResponse as $item) {
            $eventType = new EventType('', '');
            $eventType->bsonUnserialize($item->getArrayCopy());
            $result[] = $eventType;
        }

        return $result;
    }

    /**
     * @param string $id
     * @return EventType|null
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     * @throws \MongoDB\Driver\Exception\RuntimeException
     * @throws \MongoDB\Exception\InvalidArgumentException
     * @throws \MongoDB\Exception\UnsupportedException
     */
    public function selectEventTypeById(string $id): ?EventType
    {
        $connection = $this->dataSource->getConnection();
        /** @var Collection $collection */
        $collection = $connection->gametrackdb->eventType;
        /** @var BSONDocument $ev */

        $event = null;

        $ev = $collection->findOne(['_id' => new ObjectId($id)]);

        if ($ev) {
            $event = new EventType('', '');
            $event->bsonUnserialize($ev->getArrayCopy());
        }

        return $event;
    }
}

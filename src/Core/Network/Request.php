<?php

declare(strict_types=1);

namespace Core\Network;


class Request implements RequestInterface
{
    private $url;

    private $queryParams;//

    private $requestMethod;//

    private $acceptContent;

    private $contentType;

    private $contentLength;

    private $content;

    /**
     * Request constructor.
     * @param string $method
     * @param string $url
     * @param string $queryParams
     */
    public function __construct(string $method, string $url, string $queryParams = '')
    {
        $this->url = $url;
        $this->queryParams = $queryParams;
        $this->requestMethod = $method;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getQueryParams(): string
    {
        return $this->queryParams;
    }

    /**
     * @param mixed $queryParams
     */
    public function setQueryParams($queryParams): void
    {
        $this->queryParams = $queryParams;
    }

    /**
     * @return string
     */
    public function getRequestMethod(): string
    {
        return $this->requestMethod;
    }

    /**
     * @param mixed $requestMethod
     */
    public function setRequestMethod($requestMethod): void
    {
        $this->requestMethod = $requestMethod;
    }

    public function toArray(): array
    {
        return [
            'url' => $this->url,
            'queryParams' => $this->queryParams,
            'requestMethod' => $this->requestMethod,
            'content' => $this->content
        ];
    }

    public function getAcceptContent():string
    {
        return $this->acceptContent;
    }

    public function getContentType():string
    {
        return $this->contentType;
    }

    public function setAcceptContent(string $acceptContent)
    {
        $this->acceptContent = $acceptContent;
    }

    public function setContentType(string $contentType)
    {
        $this->contentType = $contentType;
    }

    public function setContentLength(string $contentLength)
    {
        $this->contentLength = $contentLength;
    }

    public function getContentLength(): string
    {
        return $this->contentLength;
    }

    public function setContent(string $content)
    {
        $this->content = $content;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}

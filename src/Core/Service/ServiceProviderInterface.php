<?php

declare(strict_types=1);

namespace Core\Service;


use Core\Container\Container;

interface ServiceProviderInterface
{
    public function register(Container $container);
}

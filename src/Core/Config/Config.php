<?php

declare(strict_types=1);

namespace Core\Config;


class Config
{
    public const DI = 'Config';

    private $config = [];

    /** @var string $env */
    private $env = 'dev';

    /**
     * Config constructor.
     * @param string $configPath
     * @param string $env
     */
    public function __construct(string $configPath, string $env)
    {
        $configPath = include($configPath);

        foreach ($configPath as $name => $value) {
            $this->config[$name] = $value;
        }

        $this->env = $env;
        $this->config['env'] = $env;
    }

    /**
     * @return array
     */
    public function getRouter(): array
    {
        $router = [];

        if (array_key_exists('router', $this->config)) {
            $router = $this->config['router'];
        }

        return $router;
    }

    /**
     * @return array|mixed
     */
    public function getDBConfig(): array
    {
        $db = [];

        if (array_key_exists('db', $this->config)) {
            $db = $this->config['db'];
        }

        return $db;
    }

    /**
     * @return array
     */
    public function getLoggerConfig(): array
    {
        $logger = [];

        if (array_key_exists('logger', $this->config)) {
            $logger = $this->config['logger'];
        }

        return $logger;
    }

    /**
     * @return array
     */
    public function getSecurityConfig(): array
    {
        $security = [];

        if (array_key_exists('security', $this->config)) {
            $security = $this->config['security'];
        }

        return $security;
    }


    /**
     * @return string
     */
    public function getEnv(): string
    {
        return $this->env;
    }
}

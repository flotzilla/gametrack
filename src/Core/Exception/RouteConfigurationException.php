<?php

namespace Core\Exception;


class RouteConfigurationException extends \Exception
{
    protected $message = "Route configuration is not valid";
}
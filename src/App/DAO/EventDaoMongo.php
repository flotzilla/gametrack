<?php

declare(strict_types=1);

namespace App\DAO;

use App\Model\Event;
use Core\DataSource\DataSource;
use MongoDB\BSON\ObjectId;
use MongoDB\Collection;
use MongoDB\Model\BSONDocument;

/**
 * Class EventDaoMongo
 * @package App\DAO
 */
class EventDaoMongo implements EventDao
{
    public const DI = 'EventDaoMongo';

    /** @var DataSource */
    private $dataSource;

    /**
     * EventDaoMongo constructor.
     * @param DataSource $dataSource
     */
    public function __construct(DataSource $dataSource)
    {
        $this->dataSource = $dataSource;
    }

    /**
     * @param Event $event
     * @return bool
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     * @throws \MongoDB\Driver\Exception\RuntimeException
     * @throws \MongoDB\Exception\BadMethodCallException
     * @throws \MongoDB\Exception\InvalidArgumentException
     */
    public function insertEvent(Event $event): bool
    {
        $connection = $this->dataSource->getConnection();
        /** @var Collection $collection */
        $collection = $connection->gametrackdb->event;
        return $collection->insertOne($event)
                ->getInsertedCount() > 0;
    }

    /**
     * @return array
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     * @throws \MongoDB\Driver\Exception\RuntimeException
     * @throws \MongoDB\Exception\InvalidArgumentException
     */
    public function selectAll(): array
    {
        $result = [];

        $connection = $this->dataSource->getConnection();
        /** @var Collection $collection */
        $collection = $connection->gametrackdb->event;
        /** @var BSONDocument $dbResponse */
        $dbResponse = $collection->find([], ['sort' => ['timestamp' => -1]])
            ->toArray() ?: [];
        foreach ($dbResponse as $item) {
            $event = new Event();
            $event->bsonUnserialize($item->getArrayCopy());
            $result[] = $event;
        }

        return $result;
    }


    /**
     * @param string $id
     * @return Event|null
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     * @throws \MongoDB\Driver\Exception\RuntimeException
     * @throws \MongoDB\Exception\InvalidArgumentException
     * @throws \MongoDB\Exception\UnsupportedException
     */
    public function selectEventById(string $id): ?Event
    {
        $connection = $this->dataSource->getConnection();
        /** @var Collection $collection */
        $collection = $connection->gametrackdb->event;
        /** @var BSONDocument $ev */

        $event = null;

        $ev = $collection->findOne(['_id' => new ObjectId($id)]);

        if ($ev) {
            $event = new Event();
            $event->bsonUnserialize($ev->getArrayCopy());
        }

        return $event;
    }
}

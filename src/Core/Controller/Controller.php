<?php

declare(strict_types=1);

namespace Core\Controller;


use Core\Exception\ControllerMethodNotFound;
use Core\Network\RequestInterface;
use Core\Network\ResponseInterface;
use Psr\Container\ContainerInterface;

/**
 * Class Controller
 * @package Core\Controller
 */
abstract class Controller
{
    public const ALLOWED_METHODS_KEY = 'allowedMethods';

    /** open all http methods by default */
    public const ALLOWED_METHODS_BY_DEFAULT = [
        'GET',
        'POST',
        'HEAD',
        'PUT',
        'DELETE',
        'CONNECT',
        'OPTIONS',
        'TRACE',
        'PATCH'
    ];

    protected $defaultContentType = 'Content-type: text/html; charset=UTF-8';

    /** @var RequestInterface $request */
    protected $request;

    /** @var ResponseInterface $response */
    protected $response;

    /** @var ContainerInterface $container */
    protected $container;

    protected $allowedMethods = [];

    protected $allowActionRewrite = false;

    /**
     * Controller constructor.
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param ContainerInterface $container
     */
    public function __construct(
        RequestInterface $request,
        ResponseInterface $response,
        ContainerInterface $container
    )
    {
        $this->request = $request;
        $this->response = $response;
        $this->container = $container;
    }

    /**
     * Call controller action (defined as nameAction in controller) or call default class method
     *
     * @param $name
     * @param $arguments
     * @return ResponseInterface|mixed
     * @throws ControllerMethodNotFound
     */
    public function __call($name, $arguments)
    {
        $method = $name . 'Action';
        if (method_exists($this, $method)) {
            $this->before();
            $body = call_user_func_array([$this, $method], $arguments);
            $this->after();
            $this->response->setBody($body ?: '');

            return $this->response;

        } else if (method_exists($this, $name)){
            // call another method
            return call_user_func_array([$this, $method], $arguments);
        } else {
            $this->after();
            throw new ControllerMethodNotFound("Method $method in controller cannot be called directly or not exist");
        }
    }

    /**
     *
     * Method can be redefined in child controllers and called before Action
     *
     * @return void
     */
    abstract protected function before(): void;

    /**
     *
     * Method can be redefined in child controllers and called after Action
     *
     * @return void
     */
    abstract protected function after(): void;

    /**
     * @param array $allowedMethods
     *
     * @return void
     */
    public function setAllowedMethods(array $allowedMethods): void
    {
        $this->allowedMethods = $allowedMethods;
    }

    /**
     * @return array
     */
    public function getAllowedMethods(): array
    {
        return $this->allowedMethods ?: self::ALLOWED_METHODS_BY_DEFAULT;
    }

    /**
     * @param RequestInterface $request
     * @return Controller
     */
    public function setRequest(RequestInterface $request): Controller
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowActionRewrite(): bool
    {
        return $this->allowActionRewrite;
    }

    /**
     * @param bool $allowActionRewrite
     */
    public function setAllowActionRewrite(bool $allowActionRewrite): void
    {
        $this->allowActionRewrite = $allowActionRewrite;
    }

    /**
     * @return string
     */
    public function getDefaultContentType(): string
    {
        return $this->defaultContentType;
    }

    /**
     * @param string $defaultContentType
     */
    public function setDefaultContentType(string $defaultContentType): void
    {
        $this->defaultContentType = $defaultContentType;
    }
}

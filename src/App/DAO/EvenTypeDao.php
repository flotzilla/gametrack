<?php

namespace App\DAO;


use App\Model\EventType;

/**
 * Interface EvenTypeDao
 * @package App\DAO
 */
interface EvenTypeDao
{
    /**
     * @param EventType $event
     * @return bool
     */
    public function insertEventType(EventType $event): bool;

    /**
     * @return array
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     * @throws \MongoDB\Driver\Exception\RuntimeException
     * @throws \MongoDB\Exception\InvalidArgumentException
     * @throws \MongoDB\Exception\UnsupportedException
     */
    public function selectAllEventTypes(): array;

    /**
     * @param string $id
     * @return EventType|null
     */
    public function selectEventTypeById(string $id): ?EventType;
}

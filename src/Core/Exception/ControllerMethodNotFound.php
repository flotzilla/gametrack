<?php

declare(strict_types=1);

namespace Core\Exception;


use RuntimeException;

class ControllerMethodNotFound extends RuntimeException
{
    protected $message = 'Method in controller cannot be called directly or not exist';
}

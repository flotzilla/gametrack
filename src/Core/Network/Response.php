<?php

declare(strict_types=1);

namespace Core\Network;


use Core\Error\Error;

/**
 * Class Response
 * @package Core\Network
 */
class Response implements ResponseInterface
{

    /** @var array $headers */
    protected $headers = [];

    /** @var string */
    protected $body = '';

    /** @var int */
    protected $statusCode;

    /** @var string */
    protected $contentType = '';

    /** @var Error[] */
    protected $errors = [];

    /** @var array */
    protected $parsedRequestParams = [];

    protected $parsedUrlParam = '';

    /**
     * Response constructor.
     * @param int $statusCode
     */
    public function __construct(int $statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @param array $h
     */
    public function setHeaders(array $h): void
    {
        $this->headers = $h;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param string $header
     */
    public function addHeader(string $header): void
    {
        $this->headers[] = $header;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body): void
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @return string
     */
    public function getContentType(): string
    {
        return $this->contentType;
    }

    /**
     * @param string $contentType
     */
    public function setContentType(string $contentType): void
    {
        $this->contentType = $contentType;
    }

    /**
     * @param array $errors
     */
    public function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param Error $error
     */
    public function addError(Error $error): void
    {
        $this->errors[] = $error;
    }

    /**
     * @return array
     */
    public function getParsedRequestParams(): array
    {
        return $this->parsedRequestParams;
    }

    /**
     * @param array $params
     */
    public function setParsedRequestParams(array $params): void
    {
        $this->parsedRequestParams = $params;
    }

    /**
     * @return string
     */
    public function getParsedUrlParam(): string
    {
        return $this->parsedUrlParam;
    }

    /**
     * @param string $parsedUrlParam
     */
    public function setParsedUrlParam(string $parsedUrlParam): void
    {
        $this->parsedUrlParam = $parsedUrlParam;
    }
}

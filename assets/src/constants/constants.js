export const APP_NAME = 'Gametrack';

export const API_SERVER_URL = 'http://127.0.0.1';
export const API_PORT = '80';

export const API_VERSION = 'V1';

export const API_SERVER = `${API_SERVER_URL}:${API_PORT}/api/${API_VERSION}/`;

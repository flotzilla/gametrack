<?php

declare(strict_types=1);

namespace Core\Service;


use Core\Controller\Controller;
use Core\Exception\ControllerMethodNotFound;
use Core\Exception\ControllerNotFoundException;
use Core\Exception\InvalidClassException;
use Core\Exception\RouteConfigurationException;
use Core\Middleware\MiddlewareInterface;
use Core\Network\RequestInterface;
use Core\Network\ResponseInterface;
use Core\Network\Router;
use Exception;
use Psr\Container\ContainerInterface;

/**
 * Class ControllerHandler
 * @package Core\Service
 */
class ControllerHandler implements MiddlewareInterface
{
    public const DI = 'ControllerHandler';

    protected const ALLOWED_METHODS = 'allowedMethods';

    /** @var Router $router */
    protected $router;

    /** @var ContainerInterface $container */
    private $container;

    /**
     * ControllerHandler constructor.
     * @param Router $router
     * @param ContainerInterface $container
     */
    public function __construct(
        Router $router,
        ContainerInterface $container)
    {
        $this->router = $router;
        $this->container = $container;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function process(
        RequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface
    {
        if ($response->getStatusCode() !== ResponseInterface::STATUS_200) {
            return $response;
        }

        /** @var Logger $logger */
        $logger = $this->container->get(Logger::DI);

        try {
            $urlRoutes = $this->router->getRouteParamsByUrl($request->getUrl());

            if (!$urlRoutes) {
                $logger->error('Route not found', $request->toArray());
                $response->setStatusCode(ResponseInterface::STATUS_404);
                return $response;
            }

            if (!array_key_exists('controller', $urlRoutes)) {
                throw new RouteConfigurationException();
            }

            $controller = $urlRoutes['controller'] . 'Controller';
            $controller = $this->convertToStudlyCaps($controller);

            if (!class_exists($controller)) {
                throw new ControllerNotFoundException('Class ' . $controller . 'not found');
            }

            /** @var Controller $controller */
            $controllerInstance = new $controller($request, $response, $this->container);

            if (!$controllerInstance instanceof Controller) {
                throw new InvalidClassException("Class $controller is not an instance of Controller");
            }

            if ($controllerInstance->isAllowActionRewrite()) {
                $action = strtolower($request->getRequestMethod());
            } else {

                if (!array_key_exists('action', $urlRoutes)) {
                    throw new RouteConfigurationException();
                }

                $action = $urlRoutes['action'];
                $action = $this->convertToCamelCase($action);
            }

            $routeAllowedMethods = array_key_exists('allowedMethods', $urlRoutes)
                ? $urlRoutes[self::ALLOWED_METHODS]
                : $controllerInstance->getAllowedMethods();

            if ($this->checkAllowedMethods($request, $routeAllowedMethods)) {
                $controllerInstance->$action();
            } else {
                $response->addHeader($controllerInstance->getDefaultContentType());
                $response->setStatusCode(ResponseInterface::STATUS_405);
            }

        } catch (RouteConfigurationException
        | ControllerNotFoundException
        | ControllerMethodNotFound
        | InvalidClassException $e
        ) {
            $logger->error($e->getMessage(), $e->getTrace());
            $response->setStatusCode(ResponseInterface::STATUS_404);
        } catch (Exception $e) {
            $logger->error($e->getMessage(), $e->getTrace());
            $response->setStatusCode(ResponseInterface::STATUS_500);
        }

        return $response;
    }

    /**
     * Convert the string with hyphens to StudlyCaps,
     * e.g. post-authors => PostAuthors
     *
     * @param string $string The string to convert
     *
     * @return string
     */
    protected function convertToStudlyCaps(string $string): string
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
    }

    /**
     * Convert the string with hyphens to camelCase,
     * e.g. add-new => addNew
     *
     * @param string $string The string to convert
     *
     * @return string
     */
    protected function convertToCamelCase(string $string): string
    {
        return lcfirst($this->convertToStudlyCaps($string));
    }

    /**
     * @param RequestInterface $request
     * @param array $allowedMethods
     * @return bool
     */
    protected function checkAllowedMethods(RequestInterface $request, array $allowedMethods): bool
    {
        return in_array($request->getRequestMethod(), $allowedMethods);
    }

}
